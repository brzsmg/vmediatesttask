package com.swlibs.apps.vmediatesttask.models

import android.util.Log
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import java.lang.reflect.Type
import java.util.*


class Pokemon : PokemonBase() {
    var url : String? = null
}

class PokemonDeserializer : JsonDeserializer<Pokemon> {

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): Pokemon {
        val p = Pokemon()
        val o = json.asJsonObject
        p.name = o.get("name").asString
        p.url = o.get("url").asString
        p.id = p.url!!.split("/").get(6).toInt()
        return p
    }

}