package com.swlibs.apps.vmediatesttask.data

import com.swlibs.apps.vmediatesttask.data.services.PokemonApi
import com.swlibs.apps.vmediatesttask.utils.HttpClient
import com.swlibs.apps.vmediatesttask.utils.Json
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class PokemonData {
    companion object {
        private var mInstance: PokemonApi? = null

        fun getInstance(): PokemonApi {
            if(mInstance == null) {
                val retrofit = Retrofit.Builder()
                    .client(HttpClient.getInstance())
                    .baseUrl("https://pokeapi.co/")
                    .addConverterFactory(GsonConverterFactory.create(Json.gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
                mInstance = retrofit.create(PokemonApi::class.java)
            }
            return mInstance as PokemonApi
        }
    }
}