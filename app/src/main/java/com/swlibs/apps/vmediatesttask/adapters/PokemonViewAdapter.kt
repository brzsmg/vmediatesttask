package com.swlibs.apps.vmediatesttask.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.swlibs.apps.vmediatesttask.R
import com.swlibs.apps.vmediatesttask.models.PokemonBase

class PokemonViewAdapter(
    val mContext : Context,
    diffUtilCallback: DiffUtil.ItemCallback<PokemonBase>,
    private val onPokemonSelected: (pokemon: PokemonBase) -> Unit
    ) : PagedListAdapter<PokemonBase, PokemonHolder>(diffUtilCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
        return PokemonHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonHolder, position: Int) {
        val item = getItem(position)!!
        holder.mvId.text = item.id.toString()
        holder.mvName.text = item.getLabelName()
        Picasso.with(mContext)
            .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + item.id + ".png")
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
            .into(holder.mvIcon)
        holder.itemView.setOnClickListener {
            onPokemonSelected.invoke(item)
        }
    }

}

class PokemonHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var mvId : TextView = itemView.findViewById(R.id.id)
    var mvName : TextView = itemView.findViewById(R.id.name)
    var mvIcon : ImageView = itemView.findViewById(R.id.icon)
}