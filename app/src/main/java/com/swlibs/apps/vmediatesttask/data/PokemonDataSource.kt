package com.swlibs.apps.vmediatesttask.data

import androidx.paging.PositionalDataSource
import com.swlibs.apps.vmediatesttask.data.PokemonRepository
import com.swlibs.apps.vmediatesttask.models.PokemonBase

class PokemonDataSource : PositionalDataSource<PokemonBase>() {

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<PokemonBase>) {
        PokemonRepository.getInstance().pokemon(params.requestedStartPosition, params.requestedLoadSize) {
            callback.onResult(it, 0)
        }
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<PokemonBase>) {
        PokemonRepository.getInstance().pokemon(params.startPosition, params.loadSize) {
            callback.onResult(it)
        }
    }

}