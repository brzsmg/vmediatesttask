package com.swlibs.apps.vmediatesttask.data

import androidx.paging.DataSource
import com.swlibs.apps.vmediatesttask.data.PokemonDataSource
import com.swlibs.apps.vmediatesttask.models.PokemonBase

class PokemonSourceFactory : DataSource.Factory<Int, PokemonBase>() {

    override fun create(): DataSource<Int, PokemonBase> {
        return PokemonDataSource()
    }

}