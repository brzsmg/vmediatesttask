package com.swlibs.apps.vmediatesttask.data

class AppData {

    private var mMaxPokemons : Int = 0

    companion object {
        private var mInstance : AppData? = null
        fun getInstance(): AppData {
            if(mInstance == null) {
                mInstance =
                    AppData()
            }
            return mInstance!!
        }
    }

    fun setMaxPokemons(max : Int) {
        mMaxPokemons = max
    }

    fun getMaxPokemons() : Int {
        return mMaxPokemons
    }

}