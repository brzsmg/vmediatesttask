package com.swlibs.apps.vmediatesttask.models.views

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.swlibs.apps.vmediatesttask.models.Pokemon


class PokemonViewModel(val context : Application) : AndroidViewModel(context) {

    var mData: MutableLiveData<ArrayList<Pokemon>> = MutableLiveData()

    fun getData() : MutableLiveData<ArrayList<Pokemon>> {
        val array = ArrayList<Pokemon>()
        //array.add(Pokemon("Rik"))
        //array.add(Pokemon("Morty"))
        mData.value = array
        return mData
    }

}