package com.swlibs.apps.vmediatesttask.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import com.swlibs.apps.vmediatesttask.R
import com.swlibs.apps.vmediatesttask.data.PokemonRepository
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_pokemon.*


class PokemonActivity : AppCompatActivity() {

    private var mId : Int = 0
    private var mRequest : Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemon)
        mId = intent.getStringExtra("id").toInt()
    }

    override fun onStart() {
        super.onStart()
        load()
    }

    fun load() {
        PokemonRepository.getInstance().pokemon(mId) { pokemon ->
            runOnUiThread {
                name.text = pokemon.name
                height.text = pokemon.height.toString()
                weight.text = pokemon.weight.toString()
                Picasso.with(baseContext)
                    .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + pokemon.id + ".png")
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .into(icon)
            }
        }
    }

}