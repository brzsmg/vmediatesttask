package com.swlibs.apps.vmediatesttask.data

import androidx.paging.DataSource
import androidx.room.*
import com.swlibs.apps.vmediatesttask.models.Pokemon
import com.swlibs.apps.vmediatesttask.models.PokemonBase

@Dao
interface PokemonDao {

        @Insert(onConflict = OnConflictStrategy.IGNORE)
        fun insert(list : List<PokemonBase>)

        @Query("SELECT * FROM pokemons ORDER BY id ASC LIMIT :count OFFSET :start ")
        fun pokemon(start: Int, count: Int) : List<PokemonBase>

        @Query("SELECT * FROM pokemons WHERE id = :id ")
        fun pokemon(id: Int) : PokemonBase

        @Update
        fun update(pokemon : PokemonBase)
}