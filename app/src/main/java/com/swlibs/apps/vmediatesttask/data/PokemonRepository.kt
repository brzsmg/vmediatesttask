package com.swlibs.apps.vmediatesttask.data

import android.content.Context
import android.util.Log
import androidx.room.Room
import com.swlibs.apps.vmediatesttask.models.PokemonBase
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.Objects


class PokemonRepository(context: Context) {

    var db = Room.databaseBuilder(context, PokemonDb::class.java, "db").build()

    private var dao: PokemonDao = db.pokemons()

    private var mRequest : Disposable? = null

    companion object {
        private var mInstance : PokemonRepository? = null

        fun getInstance() : PokemonRepository {
            Objects.requireNonNull(mInstance)
            return mInstance as PokemonRepository
        }
    }

    init {
        mInstance = this
    }

    fun pokemon(start: Int, size: Int, callback: (list: List<PokemonBase>) -> Unit)  {
        mRequest?.dispose()
        mRequest = PokemonData.getInstance().requestPokemon(start, size)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe ({ result ->
                if (result.isSuccessful) {
                    val result = result.body()!!
                    AppData.getInstance().setMaxPokemons(result.count)
                    callback.invoke(result.results)
                    dao.insert(result.results)
                }
            }, { error ->
                callback.invoke(dao.pokemon(start, size))
            })
    }

    fun pokemon(id: Int, callback: (pokemon: PokemonBase) -> Unit)  {
        mRequest?.dispose()
        mRequest = PokemonData.getInstance().requestPokemon(id)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({ result ->
                if (result.isSuccessful) {
                    val pokemon = result.body()!!
                    callback.invoke(pokemon)
                    dao.update(pokemon)
                }
            }, { error ->
                callback.invoke(dao.pokemon(id))
                Log.e("", "")
            })
    }
}