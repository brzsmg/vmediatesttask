package com.swlibs.apps.vmediatesttask.models

import android.util.Log
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

@Entity(tableName = "pokemons", indices = [Index(value = ["id"], unique = false)])
open class PokemonBase() : Serializable {
    @PrimaryKey
    open var id : Int? = null

    open var name : String? = null

    @SerializedName("is_default")
    var default : Boolean? = null

    var height : Int? = null

    var weight : Int? = null

    fun getLabelName() : String {
        return name!!.substring(0, 1).toUpperCase(Locale.getDefault()) + name!!.substring(1)
    }

}
