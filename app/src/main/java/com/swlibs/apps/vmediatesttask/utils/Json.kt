package com.swlibs.apps.vmediatesttask.utils


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import com.swlibs.apps.vmediatesttask.models.Pokemon
import com.swlibs.apps.vmediatesttask.models.PokemonDeserializer
import java.lang.reflect.Type

/**
 * Json
 */
object Json {
    private var mGson: Gson? = null
    private fun init() {
        if (mGson != null) {
            return
        }

        mGson = GsonBuilder()

            .registerTypeAdapter(Pokemon::class.java, PokemonDeserializer())
            .registerTypeAdapter(
                Float::class.java,
                JsonDeserializer { jsonElement, type, jsonDeserializationContext ->
                    var result: Float? = null
                    result = try {
                        jsonElement.asFloat
                    } catch (e: NumberFormatException) {
                        return@JsonDeserializer result
                    }
                    result
                })
            .setLenient()
            .create()
    }

    val gson: Gson
        get() {
            init()
            return mGson!!
        }

    fun <T> fromJson(json: String?, c: Class<T>?): T {
        init()
        return mGson!!.fromJson(json, c)
    }

    fun <T> fromJson(json: String?, c: Type?): T {
        init()
        return mGson!!.fromJson(json, c)
    }

    fun <T> toJson(o: T): String {
        init()
        return mGson!!.toJson(o)
    }
}