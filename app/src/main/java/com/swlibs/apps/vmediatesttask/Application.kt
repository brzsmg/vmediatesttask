package com.swlibs.apps.vmediatesttask

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.facebook.stetho.Stetho
import com.swlibs.apps.vmediatesttask.data.PokemonRepository
import com.swlibs.apps.vmediatesttask.data.services.PokemonApi
import okhttp3.OkHttpClient

val AppCompatActivity.app : Application
    get() = this.application as Application

val Fragment.app : Application
    get() = this.activity?.application as Application

class Application : android.app.Application() {

    companion object {
        private lateinit var mInstance : Application
        fun getInstance(): Application {
            return mInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        Stetho.initializeWithDefaults(this)
        PokemonRepository(this)
    }

}