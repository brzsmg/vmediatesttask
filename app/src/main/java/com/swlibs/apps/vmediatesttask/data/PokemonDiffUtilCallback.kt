package com.swlibs.apps.vmediatesttask.data

import androidx.recyclerview.widget.DiffUtil
import com.swlibs.apps.vmediatesttask.models.PokemonBase

class PokemonDiffUtilCallback : DiffUtil.ItemCallback<PokemonBase>() {
    override fun areItemsTheSame(oldItem: PokemonBase, newItem: PokemonBase): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: PokemonBase, newItem: PokemonBase): Boolean {
        return oldItem.name == newItem.name
    }

}