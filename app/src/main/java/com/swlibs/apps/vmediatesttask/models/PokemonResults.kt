package com.swlibs.apps.vmediatesttask.models

class PokemonResults(
    val count: Int,
    val next: String,
    val previous: String,
    val results: List<Pokemon>
)
