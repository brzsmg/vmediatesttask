package com.swlibs.apps.vmediatesttask.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.swlibs.apps.vmediatesttask.models.PokemonBase

@Database(
    entities = [PokemonBase::class],
    version = 1,
    exportSchema = false
)
abstract class PokemonDb : RoomDatabase() {

    abstract fun pokemons(): PokemonDao

}