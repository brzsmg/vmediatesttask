package com.swlibs.apps.vmediatesttask.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.swlibs.apps.vmediatesttask.data.AppData
import com.swlibs.apps.vmediatesttask.data.PokemonDiffUtilCallback
import com.swlibs.apps.vmediatesttask.data.PokemonSourceFactory
import com.swlibs.apps.vmediatesttask.R
import com.swlibs.apps.vmediatesttask.adapters.PokemonViewAdapter
import com.swlibs.apps.vmediatesttask.models.PokemonBase
import com.swlibs.apps.vmediatesttask.models.views.PokemonViewModel
import kotlinx.android.synthetic.main.activity_pokemons.*
import java.util.*
import java.util.concurrent.Executors

class ListActivity : AppCompatActivity() {

    private lateinit var viewModel: PokemonViewModel
    private lateinit var mPagedList: PagedList<PokemonBase>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemons)

        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(this.application).create(PokemonViewModel::class.java)

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(30)
            .build()

        val sourceFactory =
            PokemonSourceFactory()

        val builder = LivePagedListBuilder<Int, PokemonBase>(sourceFactory, config)
        builder.setFetchExecutor(Executors.newSingleThreadExecutor())
        val pagedList = builder.build()

        list_view.layoutManager = LinearLayoutManager(this)
        val adapter = PokemonViewAdapter(this,
            PokemonDiffUtilCallback()
        ) {
            val intent = Intent(this, PokemonActivity::class.java)
            intent.putExtra("id", it.id.toString())
            startActivity(intent)
        }
        pagedList.observe(this, Observer { pl ->
            mPagedList = pl;
            adapter.submitList(pl)
        })
        list_view.adapter = adapter

        random.setOnClickListener {
            val max = AppData.getInstance().getMaxPokemons()
            val random = Random(Date().time)
            val start = random.nextInt(max - 30)
            //TODO: ...
        }

        /*viewModel.getData().observe(this, Observer { pokemons ->
            run {

            }
        })*/

    }
}