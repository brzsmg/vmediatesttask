package com.swlibs.apps.vmediatesttask.utils

import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class HttpClient {
    companion object {
        private var mInstance : OkHttpClient? = null

        fun getInstance() : OkHttpClient{
            if(mInstance == null) {
                mInstance = OkHttpClient.Builder()
                    .addNetworkInterceptor(StethoInterceptor())
                    .connectTimeout(4, TimeUnit.SECONDS)
                    .readTimeout(4, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(false)
                    .build()
            }
            return mInstance as OkHttpClient
        }
    }

}