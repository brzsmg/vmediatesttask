package com.swlibs.apps.vmediatesttask.data.services

import com.swlibs.apps.vmediatesttask.models.Pokemon
import com.swlibs.apps.vmediatesttask.models.PokemonBase
import com.swlibs.apps.vmediatesttask.models.PokemonResults
import io.reactivex.Single

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonApi {

    @GET("/api/v2/pokemon/")
    fun requestPokemon(
        @Query("offset") offset : Int,
        @Query("limit") limit : Int
    ) : Single<Response<PokemonResults>>

    @GET("/api/v2/pokemon/{id}/")
    fun requestPokemon(
        @Path("id") id : Int
    ) : Single<Response<PokemonBase>>

}